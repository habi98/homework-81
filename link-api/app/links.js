const express = require('express');
const nanoid = require('nanoid');

const Link = require('../models/Link');

const router = express.Router();

router.get('/:shortUrl', (req, res) => {

    Link.findOne({shortUrl: req.params.shortUrl})
        .then(result => {
                if (result) return res.status(301).redirect(result.originalUrl);
                res.sendStatus(404);
            })
                .catch(() => res.sendStatus(500))

});


router.post('/', (req, res) => {
    const linkData = req.body;

    linkData.shortUrl = nanoid(6);

    const link = new Link(linkData);

    link.save()
        . then(result => res.send(result))
        .catch(error => res.sendStatus(400).send(error))
});


module.exports = router;

