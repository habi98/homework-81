const express =require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const links = require('./app/links');

const app = express();


app.use(express.json());
app.use(cors());
const port = 8000;


mongoose.connect('mongodb://localhost/link', {useNewUrlParser: true}).then(() => {
   app.use('/links', links);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`)
    })
});