const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LinksSchema = new Schema({
    originalUrl: {
        type: String, required: true
    },
    shortUrl: String
});

const Link = mongoose.model('Link', LinksSchema);

module.exports = Link;