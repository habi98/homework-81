import React, {Component, Fragment} from 'react';
import './FormShortenLink.css'
import {connect} from "react-redux";
import {createLink} from "../../store/action";

class FormShortenLink extends Component {
    state = {
        originalUrl: ''
    };


    valueChange = event => {
        this.setState({[event.target.name]: event.target.value})
    };
    render() {
        return (
            <Fragment>
                <div className="form">
                    <div className="shorten-title">
                        <h2 style={{fontWeight: '400'}}>Shorten your link!</h2>
                    </div>
                    <input value={this.state.originalUrl} onChange={this.valueChange} name="originalUrl" className="input" type="text"/>
                    <div className="shorten">
                        <button onClick={() => this.props.createLink({...this.state})} className="btn-shorten">Shorten!</button>
                    </div>
                    <div className="link">
                        <a href={'http://localhost:8000/links/' + this.props.link} style={{display: 'block'}}>{this.props.link}</a>
                    </div>
                </div>
            </Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        link: state.link
    }
};

const mapDispatchToProps = dispatch => ({
    createLink: link => dispatch(createLink(link))
});

export default connect(mapStateToProps, mapDispatchToProps)(FormShortenLink);