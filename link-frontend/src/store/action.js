import axios from '../axios-link';

export const CREATE_LINK_SUCCESS ='CREATE_LINK_SUCCESS';
export const createLinkSuccess = (data) => ({type: CREATE_LINK_SUCCESS, data});

export const createLink = link => {
    return dispatch => {
       return axios.post('/links', link).then(response => {
           dispatch(createLinkSuccess(response.data.shortUrl))
        })
    }
};